/*
 * spiUser.h
 *
 *  Created on: Nov 02, 2020
 *      Author: chungnt@epi-tech.com.vn
 *
 *      
*/
#ifdef __cplusplus
extern "C"
{
#endif

#ifndef SPIUSER_H
#define SPIUSER_H


/* ==================================================================== */
/* ========================== include files =========================== */
/* ==================================================================== */

/* Inclusion of system and local header files goes here */
#include "ht32.h"

/* ==================================================================== */
/* ============================ constants ============================= */
/* ==================================================================== */

/* #define and enum statements go here */
#define SPI_IDN           SPI1
#define SPI_PORT          HT_SPI1
#define SPI_IRQ           SPI1_IRQn
#define SPI_SCK_GPIO_ID   GPIO_PA
#define SPI_SCK_AFIO_PIN  AFIO_PIN_0
#define SPI_MOSI_GPIO_ID  GPIO_PA
#define SPI_MOSI_AFIO_PIN AFIO_PIN_1
#define SPI_MISO_GPIO_ID  GPIO_PA
#define SPI_MISO_AFIO_PIN AFIO_PIN_2

#define SPI_SEL1_PORTNUM  GPIO_PA
#define SPI_SEL1_PORT     HT_GPIOA
#define SPI_SEL1_PIN      GPIO_PIN_3
/* ==================================================================== */
/* ========================== public data ============================= */
/* ==================================================================== */

/* Definition of public (external) data types go here */





/* ==================================================================== */
/* ======================= public functions =========================== */
/* ==================================================================== */

/* Function prototypes for public (external) functions go here */
void SPI_User_Configuration(void);
u8 SPI_User_SendReceive(u8 data);
void SPI_User_SendMulti(u8 *p, u32 len);
void SPI_User_ReceiveMulti(u8 *p, u32 len);
#endif
#ifdef __cplusplus
}
#endif
