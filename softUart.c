/*
 * softUart.c
 *
 *  Created on: Dec 8, 2020
 *      Author: chungnt@epi-tech.com.vn
 */
#include "softUart.h"
#include <stdarg.h>
#include <stdlib.h>
#include "gpioUser.h"

#define SOFT_UART_PORTNUM_TX  GPIO_PB
#define SOFT_UART_PORT_TX     HT_GPIOB
#define SOFT_UART_PIN_TX      GPIO_PIN_10

#define SOFT_UART_PORTNUM_RX  GPIO_PB
#define SOFT_UART_PORT_RX     HT_GPIOB
#define SOFT_UART_PIN_RX      GPIO_PIN_11
#define RX_AFIO_EXTI_CH 	    AFIO_EXTI_CH_11
#define RX_EXTIO_SOURCE       AFIO_ESS_PB
#define RX_EXTI_CHANNEL 	    EXTI_CHANNEL_11
#define RX_EXTI_IRQn    	    EXTI11_IRQn


#define Baudrate       115200 //bps
#define OneBitDelay    (1000000/Baudrate)
#define Freq_Mhz       72
#define fac_us         (Freq_Mhz / 8)
#define UART_TX_PIN(x) Gpio_WriteVal_Pin(SOFT_UART_PORT_TX, SOFT_UART_PIN_TX, x?SET:RESET)
#define UART_RX_PIN    Gpio_Read_Pin(SOFT_UART_PORT_RX, SOFT_UART_PIN_RX)

volatile SWUART_Data_Bits Data_Bit = SWUART_Data_BitStart;
volatile SWUART_Typedef_t DataRX_t;

void SOFTUART_Configuration(void)
{
	CKCU_PeripClockConfig_TypeDef CKCUClock = {{0}};

	/*
	 * For TX
	*/
	GPIO_InitTypeDef gpioInitStruct;
	_GPIO_USER_GPIOB_CLK_ENABLE(CKCUClock);
	_GPIO_USER_GLOBAL_CLK_ENABLE(CKCUClock);

	gpioInitStruct.gpioPort = SOFT_UART_PORTNUM_TX;
	gpioInitStruct.afioPin = (u32)SOFT_UART_PIN_TX;
	gpioInitStruct.afioMode = AFIO_FUN_GPIO;
	gpioInitStruct.htGPIO = SOFT_UART_PORT_TX;
	gpioInitStruct.gpioPin = SOFT_UART_PIN_TX;
	gpioInitStruct.gpioDir = GPIO_DIR_OUT;
	// gpioInitStruct.gpioPull = GPIO_PR_DISABLE;
	// gpioInitStruct.ctrl = ENABLE;

	AFIO_GPxConfig(gpioInitStruct.gpioPort, gpioInitStruct.afioPin, gpioInitStruct.afioMode);
	GPIO_DirectionConfig(gpioInitStruct.htGPIO, gpioInitStruct.gpioPin, gpioInitStruct.gpioDir);
	Gpio_WriteHig_Pin(SOFT_UART_PORT_TX, SOFT_UART_PIN_TX);

	/*
	 * For RX
	*/
	/* Enable peripheral clock */
	_GPIO_USER_GPIOB_CLK_ENABLE(CKCUClock);
	_GPIO_USER_INTERRUPT_ENABLE(CKCUClock);
	_GPIO_USER_GLOBAL_CLK_ENABLE(CKCUClock);

	gpioInitStruct.gpioPort = SOFT_UART_PORTNUM_RX;
	gpioInitStruct.afioPin = (u32)SOFT_UART_PIN_RX;
	gpioInitStruct.afioMode = AFIO_FUN_GPIO;
	gpioInitStruct.htGPIO = SOFT_UART_PORT_RX;
	gpioInitStruct.gpioPin = SOFT_UART_PIN_RX;
	gpioInitStruct.gpioDir = GPIO_DIR_IN;
	gpioInitStruct.gpioPull = GPIO_PR_UP;
	gpioInitStruct.ctrl = ENABLE;

	AFIO_GPxConfig(gpioInitStruct.gpioPort, gpioInitStruct.afioPin, gpioInitStruct.afioMode);
	GPIO_PullResistorConfig(gpioInitStruct.htGPIO, gpioInitStruct.gpioPin, gpioInitStruct.gpioPull);
	GPIO_DirectionConfig(gpioInitStruct.htGPIO, gpioInitStruct.gpioPin, gpioInitStruct.gpioDir);
	GPIO_InputConfig(gpioInitStruct.htGPIO, gpioInitStruct.gpioPin, gpioInitStruct.ctrl);

	/* Select Port as EXTI Trigger Source */
	AFIO_EXTISourceConfig(RX_AFIO_EXTI_CH, RX_EXTIO_SOURCE);


	{ /* Configure EXTI Channel n as rising edge trigger */
		/* !!! NOTICE !!!
			* Notice that the local variable (structure) did not have an initial value.
			* Please confirm that there are no missing members in the parameter settings below in this function.
		*/
		EXTI_InitTypeDef EXTI_InitStruct;
		EXTI_InitStruct.EXTI_Channel = RX_EXTI_CHANNEL;
		EXTI_InitStruct.EXTI_Debounce = EXTI_DEBOUNCE_DISABLE;
		EXTI_InitStruct.EXTI_DebounceCnt = 0;
		EXTI_InitStruct.EXTI_IntType = EXTI_NEGATIVE_EDGE;
		EXTI_Init(&EXTI_InitStruct);
	}
	/* Enable EXTI & NVIC line Interrupt */
	EXTI_IntConfig(RX_EXTI_CHANNEL, ENABLE);
	NVIC_SetPriority(RX_EXTI_IRQn, 0);
	NVIC_EnableIRQ(RX_EXTI_IRQn);
	
	SOFTUART_TransmitStringValue("Soft UART baud: %d\r\n", Baudrate);
}

void SOFTUART_Transmit(const char DataValue)
{
	/* Basic Logic

	TX pin is usually high. A high to low bit is the starting bit and
	a low to high bit is the ending bit. No parity bit. No flow control.
	BitCount is the number of bits to transmit. Data is transmitted LSB first.

	*/
	uint8_t i;
	// Send Start Bit
	UART_TX_PIN(0);
	delay_us(OneBitDelay);
	for(i = 0; i < 8; i++)
	{
		//Set Data pin according to the DataValue
		if(((DataValue >> i) & 0x1) == 0x1 ) //if Bit is high
		{
			UART_TX_PIN(1);
		}
		else //if Bit is low
		{
			UART_TX_PIN(0);
		}
		delay_us(OneBitDelay);
	}

	//Send Stop Bit
	UART_TX_PIN(1);
	delay_us(OneBitDelay);
}

void SOFTUART_TransmitString(const char* str)
{
	while(*str != 0)
	{
		SOFTUART_Transmit(*str);
		str++;
	}
}

void SOFTUART_TransmitStringValue(const char* str, ...)
{
	unsigned char buffer[200];
	va_list arg;
	va_start(arg, str);
	vsprintf((char*)buffer, str, arg);
	va_end(arg);
	SOFTUART_TransmitString((const char*)buffer);
}
/*------------------------------------------------------*/
void delay_us(unsigned long time)
{
	SysTick->LOAD = time * fac_us;
	SysTick->VAL = 0;
	SysTick->CTRL = 0x01;
	while(!(SysTick->CTRL & SysTick_CTRL_COUNTFLAG_Msk));
	SysTick->CTRL = 0;
}

void EXTI11_IRQHandler(void)
{
	EXTI_ClearEdgeFlag(RX_EXTI_CHANNEL);
	delay_us(OneBitDelay/2);
	if(UART_RX_PIN == RESET) 											// check start bit
	{
		EXTI_IntConfig(RX_EXTI_CHANNEL, DISABLE);		// disable RX interrupt
		Data_Bit = SWUART_Data_Bit0;
		DataRX_t.byteRX = 0;
		SysTick->LOAD = (OneBitDelay) * fac_us; 		
		SysTick->VAL = 0;
		SysTick->CTRL = 0x03; 											// enable counter (0x01) and enable interrupt (0x02)
	}
}

void SysTick_Handler(void)
{
	// read data bit: LSB first
	DataRX_t.byteRX |= (UART_RX_PIN == RESET) ? (0) : (0x80 >> Data_Bit);

	if(--Data_Bit == SWUART_Data_BitStop) 
	{
		/* disable counter */
		SysTick->CTRL = 0;	
		/* Push data byte to buffer */
		DataRX_t.bufferRX[DataRX_t.bufferIndex++] = DataRX_t.byteRX;
		if(DataRX_t.byteRX == '\n') 
		{
			DataRX_t.bufferRX[DataRX_t.bufferIndex] = 0;
			DataRX_t.state = SWUART_State_Buffer_Frame;
		}
		else if(DataRX_t.bufferIndex == MAX_RX_LEN) 
		{
			DataRX_t.bufferRX[DataRX_t.bufferIndex] = 0;
			DataRX_t.state = SWUART_State_Buffer_Full;
		}
		delay_us(OneBitDelay/2);
		/* Enable RX interrupt for next byte */
		EXTI_IntConfig(RX_EXTI_CHANNEL, ENABLE);
	}
}
