/*
 * uartUser.c
 *
 *  Created on: Nov 01, 2020
 *      Author: chungnt@epi-tech.com.vn
*/
/* ==================================================================== */
/* ========================== include files =========================== */
/* ==================================================================== */

/* Inclusion of system and local header files goes here */
#include "uartUser.h"
#include "SIMCom_ATC.h"

/* ==================================================================== */
/* ============================ constants ============================= */
/* ==================================================================== */

/* ==================================================================== */
/* ======================== global variables ========================== */
/* ==================================================================== */

/* Global variables definitions go here */
buffer_t bufferRx0;
extern ATCommand_t SIMCom_ATC_ATCommand;

/* ==================================================================== */
/* ========================== private data ============================ */
/* ==================================================================== */

/* Definition of private datatypes go here */


/* ==================================================================== */
/* ====================== private functions =========================== */
/* ==================================================================== */

/* Function prototypes for private (static) functions go here */
static void RX0_ProcessNewData(void);

/* ==================================================================== */
/* ===================== All functions by section ===================== */
/* ==================================================================== */

/* Functions definitions go here, organised into sections */
/* ==================================================================== */
/* ============================ UxART0 ================================ */
/* ==================================================================== */
void UxART0_Configuration(void)
{
  { /* Enable peripheral clock of AFIO, UxART                                                               */
    CKCU_PeripClockConfig_TypeDef CKCUClock = {{0}};
    CKCUClock.Bit.AFIO                   = 1;
    CKCUClock.Bit.PA            			= 1;																									/* RXx_GPIO_CLK */
    CKCUClock.Bit.USART0               = 1;
    CKCU_PeripClockConfig(CKCUClock, ENABLE);
  }

  /* Turn on UxART Rx internal pull up resistor to prevent unknow state                                     */
  GPIO_PullResistorConfig(HT_GPIOA, GPIO_PIN_5, GPIO_PR_UP); 								/* RXx_GPIO_PORT & RXx_GPIO_PIN */

  /* Config AFIO mode as UxART function.                                                                    */
  AFIO_GPxConfig(GPIO_PA, GPIO_PIN_4, AFIO_FUN_USART_UART); // Tx						/* TXx_GPIO_ID & TXx_GPIO_PIN   */
  AFIO_GPxConfig(GPIO_PA, GPIO_PIN_5, AFIO_FUN_USART_UART); // Rx						/* RXx_GPIO_ID & RXx_GPIO_PIN   */

  {
    /* UxART configured as follow:
          - BaudRate = 115200 baud
          - Word Length = 8 Bits
          - One Stop Bit
          - None parity bit
    */
    USART_InitTypeDef USART_InitStructure = {0};
    USART_InitStructure.USART_BaudRate = 115200;
    USART_InitStructure.USART_WordLength = USART_WORDLENGTH_8B;
    USART_InitStructure.USART_StopBits = USART_STOPBITS_1;
    USART_InitStructure.USART_Parity = USART_PARITY_NO;
    USART_InitStructure.USART_Mode = USART_MODE_NORMAL;
    USART_Init(HT_USART0, &USART_InitStructure);
  }

  /* Enable UxART interrupt of NVIC                                                                         */
  NVIC_EnableIRQ(USART0_IRQn);

  /* Enable UxART Rx interrupt                                                                              */
  USART_IntConfig(HT_USART0, USART_INT_RXDR, ENABLE);

  /* Enable UxART Tx and Rx function                                                                        */
  USART_TxCmd(HT_USART0, ENABLE);
  USART_RxCmd(HT_USART0, ENABLE);
}

void UxART0_SendByte(uint8_t xByte)
{
    USART_SendData(HT_USART0, xByte);
		while(USART_GetFlagStatus(HT_USART0, USART_FLAG_TXC) == RESET);
}

void UxART0_SendString(uint8_t *str)
{
    while(*str != 0)
    {
        UxART0_SendByte(*str);
        str++; 
    }
}

/*************************************************************************************************************
  * @brief  For UxART RX
  * @retval None
  ***********************************************************************************************************/
void UxART0_Process(void)
{
	if(bufferRx0.State > 0)
	{
		bufferRx0.State--;
		if(bufferRx0.State == 0)
		{
			RX0_ProcessNewData();
		}
	}
}

void USART0_IRQHandler(void)
{
    if (USART_GetFlagStatus(HT_USART0, USART_FLAG_RXDR))
    {
        bufferRx0.Data[bufferRx0.Index] = USART_ReceiveData(HT_USART0);
        bufferRx0.Index++;
        bufferRx0.State = 3;
    }
}

static void RX0_ProcessNewData(void)
{
	bufferRx0.Data[bufferRx0.Index] = 0;
	DEBUG("Send to SIMCom: %s", bufferRx0.Data);
	UxART1_SendString(bufferRx0.Data);
	// printf("%s\r\n", bufferRx.Data);
	if(bufferRx0.Data[0] == '[' && bufferRx0.Data[bufferRx0.Index-1] == ']')
	{
		
	}
	// else printf("command not support\r\n");

	bufferRx0.Index = 0;
}

/* ==================================================================== */
/* ============================ UxART1 ================================ */
/* ==================================================================== */
void UxART1_Configuration(void)
{
  { /* Enable peripheral clock of AFIO, UxART                                                               */
    CKCU_PeripClockConfig_TypeDef CKCUClock = {{0}};
    CKCUClock.Bit.AFIO                   = 1;
    CKCUClock.Bit.PB            			= 1;																									/* RXx_GPIO_CLK */
    CKCUClock.Bit.UART1               = 1;
    CKCU_PeripClockConfig(CKCUClock, ENABLE);
  }

  /* Turn on UxART Rx internal pull up resistor to prevent unknow state                                     */
  GPIO_PullResistorConfig(HT_GPIOB, GPIO_PIN_5, GPIO_PR_UP); 								/* RXx_GPIO_PORT & RXx_GPIO_PIN */

  /* Config AFIO mode as UxART function.                                                                    */
  AFIO_GPxConfig(GPIO_PB, GPIO_PIN_4, AFIO_FUN_USART_UART); // Tx						/* TXx_GPIO_ID & TXx_GPIO_PIN   */
  AFIO_GPxConfig(GPIO_PB, GPIO_PIN_5, AFIO_FUN_USART_UART); // Rx						/* RXx_GPIO_ID & RXx_GPIO_PIN   */

  {
    /* UxART configured as follow:
          - BaudRate = 115200 baud
          - Word Length = 8 Bits
          - One Stop Bit
          - None parity bit
    */
    USART_InitTypeDef USART_InitStructure = {0};
    USART_InitStructure.USART_BaudRate = 115200;
    USART_InitStructure.USART_WordLength = USART_WORDLENGTH_8B;
    USART_InitStructure.USART_StopBits = USART_STOPBITS_1;
    USART_InitStructure.USART_Parity = USART_PARITY_NO;
    USART_InitStructure.USART_Mode = USART_MODE_NORMAL;
    USART_Init(HT_UART1, &USART_InitStructure);
  }

  /* Enable UxART interrupt of NVIC                                                                         */
  NVIC_EnableIRQ(UART1_IRQn);

  /* Enable UxART Rx interrupt                                                                              */
  USART_IntConfig(HT_UART1, USART_INT_RXDR, ENABLE);

  /* Enable UxART Tx and Rx function                                                                        */
  USART_TxCmd(HT_UART1, ENABLE);
  USART_RxCmd(HT_UART1, ENABLE);
}

void UxART1_SendByte(uint8_t xByte)
{
    USART_SendData(HT_UART1, xByte);
		while(USART_GetFlagStatus(HT_UART1, USART_FLAG_TXC) == RESET);
}

void UxART1_SendString(uint8_t *str)
{
    while(*str != 0)
    {
        UxART1_SendByte(*str);
        str++; 
    }
}

/*************************************************************************************************************
  * @brief  For UxART RX
  * @retval None
  ***********************************************************************************************************/
void UxART1_Process(void)
{
	if(SIMCom_ATC_ATCommand.ReceiveBuffer.State > 0)
	{
		SIMCom_ATC_ATCommand.ReceiveBuffer.State--;
		if(SIMCom_ATC_ATCommand.ReceiveBuffer.State == 0)
		{
			SIMCom_ATC_ProcessData();
		}
	}
}

void UART1_IRQHandler(void)
{
    if (USART_GetFlagStatus(HT_UART1, USART_FLAG_RXDR))
    {
        SIMCom_ATC_ATCommand.ReceiveBuffer.Buffer[SIMCom_ATC_ATCommand.ReceiveBuffer.BufferIndex] = USART_ReceiveData(HT_UART1);
        SIMCom_ATC_ATCommand.ReceiveBuffer.BufferIndex++;
        SIMCom_ATC_ATCommand.ReceiveBuffer.State = 3;
    }
}
